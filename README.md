# Simple Web Service using Elixir Phoenix

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000/api/users`](http://localhost:4000/api/users) from your browser.
 # API Endpoints
```ssh
GET /api/users
```
```ssh
GET /api/users/:id
```
```ssh
POST /api/users/
```
### Using CURL
```curl
curl -H "Content-Type: application/json" -X POST -d '{"user":{"email":"some@email.com","age":18}}' http://localhost:4000/api/users
```
### Sending JSON
```json
{
    "user": {
        "name": "Darwin",
        "email": "darwin@email.com",
        "age": 22
    }
}
```
```ssh
PATCH /api/users/:id
```
```ssh
PUT /api/users/:id
```
```ssh
DELETE /api/users/:id
```