defmodule MywebserviceWeb.Router do
  use MywebserviceWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", MywebserviceWeb do
    pipe_through(:api)

    resources("/users", UserController, except: [:new, :edit])
  end
end
