defmodule MywebserviceWeb.UserController do
  use MywebserviceWeb, :controller

  alias Mywebservice.Account
  alias Mywebservice.Account.User

  action_fallback(MywebserviceWeb.FallbackController)

  def index(conn, _params) do
    users = Account.list_users()
    render(conn, "index.json", users: users)
  end

  def create(conn, %{"user" => user_params}) do
    with {:ok, %User{} = user} <- Account.create_user(user_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", user_path(conn, :show, user))
      |> render("show.json", user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Account.get_user!(id)
    user_exist(conn, user)
    # if user !== nil do
    #   render(conn, "show.json", user: user)
    # else
    #   render(conn, "user_not_found.json")
    # end
  end

  def user_exist(conn, nil) do
    render(conn, "user_not_found.json")
  end

  def user_exist(conn, user) do
    render(conn, "show.json", user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Account.get_user!(id)

    with {:ok, %User{} = user} <- Account.update_user(user, user_params) do
      render(conn, "show.json", user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Account.get_user!(id)

    with {:ok, %User{}} <- Account.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
