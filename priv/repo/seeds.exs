# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Mywebservice.Repo.insert!(%Mywebservice.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Mywebservice.Account.User
alias Mywebservice.Repo

%User{name: "darwin", email: "darwin@email.com", age: 5} |> Repo.insert!()
%User{name: "julius", email: "julius@email.com",, age: 15} |> Repo.insert!()
%User{name: "neil", email: "neil@email.com", age: 25} |> Repo.insert!()
%User{name: "rafael", email: "rafael@email.com", age: 35} |> Repo.insert!()
%User{name: "alistair", email: "alistair@email.com", age: 55} |> Repo.insert!()
